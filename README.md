# Un test de conversation framework MVC POO
## Test pour **rigoler**

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error ex magnam temporibus harum dolores adipisci! Quod nesciunt exercitationem expedita. Enim illo libero repudiandae magnam voluptatem, dolore explicabo ut beatae molestiae!

  1. Item de liste
  2. Item de liste aussi

## Arborescence de mon projet :
  * www: dossier racine
    - index.php
    - .htaccess
  - app:dossier de l'application
    - controleurs
    - modeles
    - routeurs
    - vues
        - templates
  - noyau: dosier du framework
